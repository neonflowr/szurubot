import requests
import logging
import time
import sys
import urllib.parse
from math import floor
from func.config import CONFIG
from func import cache


QUERY_STRING = urllib.parse.quote(CONFIG["QUERY"])

def request_api(url):
    retries = 0

    while retries < CONFIG["RETRY_COUNT"]:
        try:
            return requests.get(
                url=url,
                timeout=CONFIG["REST_API_TIMEOUT"],
                auth=(CONFIG["BOORU_USERNAME"], CONFIG["BOORU_PASSWORD"]),
                headers={
                    "Content-Type": "application/json",
                    "Accept": "application/json",
                },
            ).json()
        except:
            logging.error("Request_api threw an error:", sys.exc_info()[0])
            retries = retries + 1
            time.sleep(CONFIG["RETRY_TIMEOUT"])

    return None


def get_post(id):
    data = request_api(CONFIG["BASE_URL"] + "/api/post/" + str(id))
    return data

def is_post_valid(idx):
    try:
        response = request_api(
            CONFIG["BASE_URL"]
            + "/api/posts"
            + "?offset=0"
            + "&limit=1"
            + "&query="
            + QUERY_STRING
            + " id:" + str(idx)
        )

        if not response:
            raise Exception

        is_post_in_query = response.get("results") != []
        if not is_post_in_query:
            return False

        post = response.get("results")[0]

        if CONFIG["REQUIRE_CATEGORIES"]:
            matched_tags = [
                i
                for i in range(len(post["tags"]))
                if (post["tags"][i]["category"] in CONFIG["REQUIRE_CATEGORIES"])
            ]
            if not bool(matched_tags):
                return False

        if CONFIG["REQUIRE_SOURCE"]:
            if not bool(post["source"]):
                return False

        return True
    except Exception as e:
        logging.error(str(e) + "\nFailed to check post validity: " + str(idx))
        return False

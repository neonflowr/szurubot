import time
from func.config import CONFIG
from func import file


class CacheItem:
    def __init__(self, key, value, expire_date) -> None:
        self.key = key
        self.value = value
        self.expire_date = expire_date


class Cache:
    def __init__(self, length: int) -> None:
        self.hash = {}
        self.items = []
        self.length = length

    def insert(self, item: CacheItem) -> None:
        if item.key in self.hash:
            self.remove(item.key)

        if len(self.items) > self.length:
            self.remove(self.items[-1].key)
        self.hash[item.key] = item.__dict__
        self.items.insert(0, item.__dict__)

        file.write(_CACHE_PATH, self.__dict__)

    def flush(self) -> None:
        self.hash = {}
        self.items = []
        file.write(_CACHE_PATH, self.__dict__)

    def remove(self, key) -> None:
        del self.hash[key]
        index = [i for i in range(len(self.items)) if self.items[i]["key"] == key][0]
        del self.items[index]
        file.write(_CACHE_PATH, self.__dict__)


_CACHE_PATH = "./data/cache.json"

_CACHE = Cache(100)
if not file.has(_CACHE_PATH):
    file.write(_CACHE_PATH, _CACHE.__dict__)
else:
    data = file.read(_CACHE_PATH)
    if data:
        _CACHE.hash = data["hash"]
        _CACHE.items = data["items"]


def flush() -> None:
    _CACHE.flush()


def has(key):
    return key in _CACHE.hash


def is_expired(key) -> bool:
    if has(key):
        val = time.time() > _CACHE.hash[key]["expire_date"]
        return val
    return


def get(key):
    if has(key):
        if is_expired(key):
            return None
        return _CACHE.hash[key]["value"]
    return None


def remove(key):
    _CACHE.remove(key)


def insert(key, value, expire_date):
    _CACHE.insert(CacheItem(key, value, expire_date))

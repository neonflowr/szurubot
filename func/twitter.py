import tweepy
import requests
import urllib.parse
import logging
import os
import json
import time
import func.api
import func.file
import re
from pathlib import Path
from func.config import CONFIG
from func import cache
from flask import Flask, request
from waitress import serve

POSTED_DATA_PATH = (
    CONFIG["POSTED_FILES_STORAGE_PATH"]
    if CONFIG["ENV"] != "test"
    else "./data/TEST_POSTED_DATA.json"
)

# Controls interactions with the Twitter API
twitter_api = None
twitter_api_v2 = None

def getPostedData():
    return func.file.read(POSTED_DATA_PATH)


def initPostedData():
    DEFAULT_POSTED_DATA = {"posts": [], "last_post_time": None, "index": 1}
    if not func.file.has(POSTED_DATA_PATH):
        func.file.write(POSTED_DATA_PATH, DEFAULT_POSTED_DATA)
    else:
        data = getPostedData()

        if not data.get("index"):
            data["index"] = data["posts"][-1]
            func.file.write(POSTED_DATA_PATH, data)

        if not data:
            func.file.write(POSTED_DATA_PATH, DEFAULT_POSTED_DATA)


def appendToPosted(id, post_time):
    data = getPostedData()

    data["posts"].append(id)
    data["last_post_time"] = post_time

    func.file.write(POSTED_DATA_PATH, data)


def getIndex() -> int:
    data = getPostedData()
    return data["index"]


def setIndex(id) -> None:
    data = getPostedData()
    data["index"] = id
    func.file.write(POSTED_DATA_PATH, data)
    logging.info("Posting index set to: " + str(id))


def get_tweet_text(data):
    text = CONFIG["TWEET_FORMAT_STRING"]
    keys = re.findall("\{([^}]*)\}", text)

    tags = []
    tag_categories = {}
    for tag in data["tags"]:
        if tag["category"] not in [*tag_categories]:
            tag_categories[tag["category"]] = []

        tag_categories[tag["category"]].append(tag["names"][0])
        tags.append(tag["names"][0])

    for key in keys:
        val = eval(
            key.strip(),
            None,
            {"post": data, "categories": tag_categories, "tags": tags},
        )

        if isinstance(val, list):
            val = val[0]

        # Handle user-defined tag alias in the config file (NOT THE ONES DEFINED BY SZURUBOORU)
        matching_tag = [
            index
            for index in range(len(CONFIG["TAG_ALIAS"]))
            if CONFIG["TAG_ALIAS"][index]["name"] in val
        ]
        if matching_tag:
            for index, item in enumerate(matching_tag):
                val = val.replace(
                    CONFIG["TAG_ALIAS"][matching_tag[index]]["name"],
                    CONFIG["TAG_ALIAS"][matching_tag[index]]["alias"],
                )

        text = text.replace("{{{key}}}".format(key=key), val)

    # Shorten URLs if enabled
    urls = re.findall(
        r"(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))",
        text,
    )

    for url in urls:
        if CONFIG["USE_URL_SHORTENER"] and (
            urllib.parse.urlparse(url[0]).netloc.replace("www.", "")
            in CONFIG["SHORTEN_DOMAINS"]
            or "*" in CONFIG["SHORTEN_DOMAINS"]
        ):
            try:
                params = "?username={username}&password={password}&action=shorturl&format=json&url={url}".format(
                    username=urllib.parse.quote(CONFIG["YOURLS_USERNAME"]),
                    password=urllib.parse.quote(CONFIG["YOURLS_PASSWORD"]),
                    url=urllib.parse.quote(url[0]),
                )

                # Shorten the source URL
                res = requests.get(
                    url=CONFIG["YOURLS_URL"] + "/yourls-api.php" + params,
                    timeout=CONFIG["REST_API_TIMEOUT"],
                    headers={"Accept": "application/json"},
                ).json()

                if not res or res["statusCode"] != 200:
                    logging.error("Failed to shorten URL for post " + str(data["id"]))
                    return None

                text = text.replace(url[0], res["shorturl"])
            except:
                logging.error("Failed to shorten URL for post " + str(data["id"]))
                return None

    return text


def post(id, dry_run):
    try:
        data = func.api.get_post(id)

        if not data:
            logging.error("Failed to get data for post with ID: " + str(id))
            return None

        if Path(data["contentUrl"]).suffix == ".mp4":
            logging.error("Tweepy doesn't support video uploads right now...")
            return None

        # Download the image and upload it
        request = requests.get(
            CONFIG["BASE_URL"] + "/" + data["contentUrl"], stream=True
        )
        if request.status_code == 200:
            LOCAL_IMAGE_PATH = "./data/temp" + Path(data["contentUrl"]).suffix

            if not dry_run:
                with open(LOCAL_IMAGE_PATH, "wb") as image:
                    for chunk in request:
                        image.write(chunk)

            text = get_tweet_text(data)
            if not text:
                logging.error("Failed to get Tweet text for post " + str(data["id"]))
                return

            if not dry_run:
                res = twitter_api.media_upload(LOCAL_IMAGE_PATH)
                twitter_api_v2.create_tweet(text=text, media_ids=[res.media_id])
                os.remove(LOCAL_IMAGE_PATH)

            current_time = time.time()
            logging.info(
                ("(DRY RUN) " if dry_run else "")
                + "Tweeted post "
                + str(data["id"])
                + " at "
                + time.strftime("%D %H:%M:%S", time.localtime(current_time))
                + ". Next Tweet is scheduled at "
                + time.strftime(
                    "%D %H:%M:%S",
                    time.localtime(current_time + CONFIG["POST_INTERVAL"]),
                )
            )

            logging.info("Tweet text:\n" + text)

            if not dry_run:
                appendToPosted(data["id"], current_time)
                setIndex(data["id"] + 1)

            return True
        else:
            logging.error("Failed to download image for post: " + str(data["id"]))
            return False
    except Exception as e:
        logging.error(str(e) + "\nFailed to tweet post: " + str(data["id"]))
        return False


def hasPostIntervalPassed(posted_data) -> bool:
    if posted_data.get("last_post_time"):
        difference = int(time.time() - posted_data["last_post_time"])
        return difference > CONFIG["POST_INTERVAL"]
    return True


def startPostLoop():
    logging.info("Starting post loop...")

    retry_count = 0

    while True:
        logging.info("Preparing to post...")
        posted_data = getPostedData()

        if not hasPostIntervalPassed(posted_data):
            logging.error(
                "Not enough time has passed since the last post at "
                + time.strftime(
                    "%D %H:%M", time.localtime(posted_data["last_post_time"])
                )
            )
            next_post_time = posted_data["last_post_time"] + CONFIG["POST_INTERVAL"]
            sleep_time = next_post_time - time.time()

            if sleep_time > 0:
                logging.info(
                    "Sleeping for "
                    + time.strftime("%Hh%Mm", time.gmtime(sleep_time))
                    + " until the next post time at "
                    + time.strftime("%D %H:%M", time.localtime(next_post_time))
                )
                time.sleep(sleep_time)

                continue

        id_to_post = posted_data["index"]

        if not func.api.is_post_valid(id_to_post):
            logging.info(
                "Post {id} is not a valid post. Skipping to {next_id}".format(
                    id=id_to_post, next_id=id_to_post + 1
                )
            )
            setIndex(id_to_post + 1)
            time.sleep(3)
            continue

        if id_to_post in posted_data["posts"]:
            logging.error(
                "Post {id} is already posted. Skipping to {next_id}".format(
                    id=id_to_post, next_id=id_to_post + 1
                )
            )
            setIndex(id_to_post + 1)
            continue

        is_successfully_tweeted = post(id_to_post, CONFIG["ENV"] == "test")

        if not is_successfully_tweeted:
            if retry_count >= CONFIG["TWEET_RETRY_COUNT"]:
                retry_count = 0
                logging.error(
                    "Reached max retries count while trying to tweet post {id}. Skipping to {next_id}...".format(
                        id=id_to_post, next_id=id_to_post + 1
                    )
                )
                setIndex(id_to_post + 1)
                continue
            else:
                logging.error(
                    "Will retry to tweet post {id} in 3 seconds".format(id=id_to_post)
                )
                time.sleep(3)
                retry_count += 1
                continue

        time.sleep(CONFIG["POST_INTERVAL"] + 1) # HACK: One second buffer so the timing check don't break


def authenticate(callback):
    global twitter_api
    global twitter_api_v2
    auth = tweepy.OAuthHandler(CONFIG["API_KEY"], CONFIG["API_KEY_SECRET"])

    # Authenticate the Twitter user
    if CONFIG.get("ACCESS_TOKEN") and CONFIG.get("ACCESS_TOKEN_SECRET"):
        auth.set_access_token(CONFIG["ACCESS_TOKEN"], CONFIG["ACCESS_TOKEN_SECRET"])

        twitter_api = tweepy.API(auth)

        twitter_api_v2 = tweepy.Client(
            consumer_key = CONFIG["API_KEY"],
            consumer_secret = CONFIG["API_KEY_SECRET"],
            access_token = CONFIG["ACCESS_TOKEN"],
            access_token_secret = CONFIG["ACCESS_TOKEN_SECRET"],
        )

        logging.info("Authenticated from defined access tokens in config file")
        callback()
    elif os.path.isfile(CONFIG["ACCESS_TOKEN_STORAGE_PATH"]):
        with open(CONFIG["ACCESS_TOKEN_STORAGE_PATH"], "r") as f:
            data = json.load(f)
            if data.get("access_token") and data.get("access_token_secret"):
                auth.set_access_token(data["access_token"], data["access_token_secret"])
                twitter_api = tweepy.API(auth)
                f.close()
                logging.info("Authenticated from stored access tokens")
                callback()
            else:
                logging.error("Stored tokens are invalid")
                f.close()
                quit()
    else:
        logging.info(
            "Running auth server at "
            + CONFIG["HOSTNAME"]
            + " on port "
            + CONFIG["PORT"]
        )

        app = Flask(__name__)

        @app.route("/oauth")
        def verifyOauth():
            global twitter_api
            if twitter_api:
                # HACK: It'd be great to stop the Flask web server after authentication, but right now
                # we're just disabling this route once finished
                return "You are already authenticated"

            verifier = request.args.get("oauth_verifier")

            auth.get_access_token(verifier)

            # Store the user's access tokens
            with open(CONFIG["ACCESS_TOKEN_STORAGE_PATH"], "w+") as f:
                data = {
                    "access_token": auth.access_token,
                    "access_token_secret": auth.access_token_secret,
                }
                json.dump(data, f)

            logging.info("Successfully authenticated user")

            twitter_api = tweepy.API(auth)

            return "You have been successfully authenticated. Restart the script to start posting from your bot."

        redirect_url = auth.get_authorization_url(False)
        print("Please go to " + redirect_url + " to authorize the application")
        serve(app, host=CONFIG["HOSTNAME"], port=CONFIG["PORT"])
